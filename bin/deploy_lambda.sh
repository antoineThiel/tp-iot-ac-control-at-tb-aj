echo "install terraform" && \
yum install -y yum-utils  && \
yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo  && \
yum -y install terraform  && \
echo "go to terraform dir"  && \
cd terraform  && \
echo "rm terraform state"  && \
rm -rf .terraform  && \
echo "init terraform"  && \
terraform init -backend-config="access_key=$AWS_ACCESS_KEY_ID" -backend-config="secret_key=$AWS_SECRET_ACCESS_KEY" -backend-config="region=eu-west-1"  && \
echo "apply terraform"  && \
terraform apply -auto-approve  && \
echo "go to .. folder"  && \
cd ..  && \
echo "execute lambda function"  && \
aws lambda update-function-code  --function-name ac_control_lambda  --zip-file fileb://lambda_function_payload.zip --region eu-west-1