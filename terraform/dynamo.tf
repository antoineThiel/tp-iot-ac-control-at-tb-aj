# aws_dynamodb_table
resource "aws_dynamodb_table" "dynamotable" {
      name = "Temperature"
      hash_key = "id"
      read_capacity = 5
      write_capacity = 5
        
      attribute {
        name = "id"
        type = "S"
      }
}